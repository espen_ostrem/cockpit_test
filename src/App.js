import React from 'react';
import './App.scss';
import Panel from './components/panel/Panel';

function App() {
	return (
		<div className="app">
			<Panel/>
		</div>
	);
}

export default App;
