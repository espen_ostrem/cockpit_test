function getRandom(max) {
	return Math.floor(Math.random()*max) + 1;
}

function chance(percentage) {
	var chance = getRandom(100);
	if(chance < percentage) return true;
	return false;
}

function getPsuData(percentageFailed) {
	if(chance(percentageFailed)) {
		return {
			status: "no",
			volt: "Power Supply AC lost"	
		}
	}
	return {
		status: "ok",
		volt: 230 + (getRandom(20) - 10)
	};
}

function getMgmtData(percentageFailed) {
	if(chance(percentageFailed)) {
		return {
			link_status: "no link",
			bps: 0
		}
	}
	return {
		link_status: "10000MBit",
		bps: 140000 + (getRandom(2000) - 1000)
	};
}

function getDataData(percentageFailed) {
	if(chance(percentageFailed)) {
		return {
			link_status: "no link",
			bps: 0
		}
	}
	return {
		link_status: "10000MBit",
		bps: 400000 + (getRandom(20000) - 10000)
	};
}

function panelFeed() {
	console.log("panelFeed");
	const feed = {
		psu1: getPsuData(5),
		psu2: getPsuData(5),
		mgmt1: getMgmtData(5),
		mgmt2: getMgmtData(15),
		data1: getDataData(5),
		data2: getDataData(10)
	};
	return feed;
};

export default panelFeed;