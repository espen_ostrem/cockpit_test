function diskFeed() {
	return fetch("file:/usr/share/testdata/")
//		.then((response) => response.json())
		.then((responseJson) => {
			console.log(responseJson);
			return responseJson;
		})
		.catch((error) => {
			console.error(error);
			return error;
		});

};

export default diskFeed;