import React, { useState, useEffect } from 'react';
import './Panel.scss';
import frontpanel from './440_frontpanel.png';
import panelFeed from '../../utils/panelFeed';
import diskFeed from '../../utils/diskFeed';
import Unit from '../unit/unit';
import { isDev } from '../../utils/utils';

function Panel() {
	const [data, setData] = useState([panelFeed()]);

	useEffect(() => {
		const interval = setInterval(() => {
			const feed = isDev() ? panelFeed() : diskFeed();
			console.log("Panel get data. feed:", feed)
			var oldArray = data.concat(feed);
			if(oldArray.length > 10) oldArray.shift();
			setData(oldArray);
		}, 1000);
		return () => clearInterval(interval);
	}, [data]);

	var sortedData = {
		psu1: data?.map(d => {return {status: d?.psu1?.status, value: d?.psu1?.volt}}),
		psu2: data?.map(d => {return {status: d?.psu2?.status, value: d?.psu2?.volt}}),
		mgmt1: data?.map(d => {return {status: d?.mgmt1?.link_status, value: d?.mgmt1?.bps}}),
		mgmt2: data?.map(d => {return {status: d?.mgmt2?.link_status, value: d?.mgmt2?.bps}}),
		data1: data?.map(d => {return {status: d?.data1?.link_status, value: d?.data1?.bps}}),
		data2: data?.map(d => {return {status: d?.data2?.link_status, value: d?.data2?.bps}}),
	}

	return (
		<div className="container">
			<img src={frontpanel} alt="frontpanel" />
			<div className="psu1">
				<Unit data={sortedData?.psu1} label="PSU1"/>
			</div>
			<div className="psu2">
				<Unit data={sortedData?.psu2} label="PSU2"/>
			</div>
			<div className="mgmt1">
				<Unit data={sortedData?.mgmt1} label="MGMT1" valueLabel="Link value"/>
			</div>
			<div className="mgmt2">
				<Unit data={sortedData?.mgmt2} label="MGMT2" valueLabel="Link value"/>
			</div>
			<div className="data1">
				<Unit data={sortedData?.data1} label="DATA1" valueLabel="Link value"/>
			</div>
			<div className="data2">
				<Unit data={sortedData?.data2} label="DATA2" valueLabel="Link value"/>
			</div>
		</div>
	);
}

export default Panel;
