import React from 'react';
import './graph.scss';
import LineChart from 'react-linechart';

function Graph({ data }) {
	const graphdata = [
		{
			color: "black",
			points: data?.map((d, index) => {return {x: index, y: d}}).filter(d => d.y !== undefined)
		}
	];

	return (
		<div className="graph">
			Chart
			<LineChart
				width={250}
				height={150}
				data={graphdata}
				xLabel="test"
				hideXLabel={true}
				hideYLabel={true}
				hideXAxis={true}
				hideYAxis={true}
				xMax="10"
				margins={{top: 0, right: 0, bottom: 0, left: 0 }}
			/>
		</div>
	);
}

Graph.propTypes = {
};

Graph.defaultProps = {
};

export default Graph;
