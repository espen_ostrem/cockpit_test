import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './unit.scss';
import { Collapse } from 'react-collapse';
import Graph from '../graph/graph';

function Unit({ label, data, valueLabel }) {
	const [viewGraph, setViewGraph] = useState(false);

	const status = data?.[data?.length - 1]?.status;
	const value = data?.[data?.length - 1]?.value;

	const graphData = data.map(d => {
		return d?.value === parseInt(d?.value, 10) ? d?.value : undefined}
	);

	return (
		<div className="unit">
			<div className="label">
				Name: {label}
			</div>

			<div className="status">
				status: {status}
			</div>
			<div className="value">
				{valueLabel}: {value}
			</div>
			<Collapse isOpened={viewGraph}>
				<Graph data={graphData}/>
			</Collapse>
			<button onClick={() => setViewGraph(!viewGraph)}>{viewGraph ? "Hide" : "Show more"}</button> 
		</div>
	);
}

Unit.propTypes = {
	label: PropTypes.string.isRequired,
	data: PropTypes.array.isRequired,
	valueLabel: PropTypes.string
};

Unit.defaultProps = {
	valueLabel: "Value"
};

export default Unit;
