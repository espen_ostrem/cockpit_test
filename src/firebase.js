// src/firebase.js
import firebase from 'firebase'
const config = {
	apiKey: "AIzaSyAY4NIWQQSKXpsV_XSCC6jf6L4sbvRBhOQ",
	authDomain: "cockpit-dfaea.firebaseapp.com",
	databaseURL: "https://cockpit-dfaea.firebaseio.com",
	projectId: "cockpit-dfaea",
	storageBucket: "cockpit-dfaea.appspot.com",
	messagingSenderId: "316812393227",
	appId: "1:316812393227:web:f853ba925f0a239b17800e",
	measurementId: "G-DE3JWCQKKR"
};
firebase.initializeApp(config);
export default firebase;